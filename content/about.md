---
title: "About"
date: 2019-12-29T08:55:44-06:00
draft: false
---

Hello! My name is Dylan Zahn and I am aspiring to have a career in IT. I'm not sure where it will lead me, but I know this is what I want to do with my life.
I have a passion for it particularly open-source software and servers. I run my own Proxmox hypervisor at home that allows me to tinker and learn about various technologies.
This blog is my way of showing what I've created and learned to the world and hopefully help me achieve my dream of having a career in IT. 


