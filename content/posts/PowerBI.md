---
title: "First Experience with Power BI"
date: 2020-02-17T09:19:29-06:00
draft: false
toc: false
images:
tags: 
  - PowerBI
  - Data
---

# Something New

I learned late last week that I have another job interview. It's for Business Intelligence development. While I have heard the term Business Intelligence is, I wasn't very familiar with what it was other than in the realm of data science. I wanted to learn as much as I can and rehash my SQL knowledge from my education. The job description also mentioned Power BI, which I have never used before now. The desktop version is free, which I was thankful for. I also found some Youtube videos explaining the basics and I glanced over the documentation a bit. It's pretty similar to other Microsoft Office products in the way items are laid out, which will help tremendously in getting up to speed on the software.

My main problem was I had no idea what kind of project I could do to set myself up for the interview. I landed on fantasy football statistics because a good number of my Twitter followers always discuss it and I enjoy reading what they come up with in terms of strategy or algorithms. I found a great resource in [Pro Football Reference](https://www.pro-football-reference.com/) and that lead me to getting offensive stats as a data source for Power BI. I knew from my limited knowledge of fantasy football that I would need the passing, rushing, and receiving stats combined to make anything useful. Going in, I didn't have a source for the actual scoring formula, but I found one from [here](https://fantasydata.com/api/fantasy-scoring-system/nfl). I also came across a resource for this exact topic from the [Power BI website](https://powerbi.microsoft.com/en-us/blog/use-power-query-to-draft-a-fantasy-football-team/).

That resource is pretty old and things have changed so I tried to do most of it on my own and only consulted that resource when I was truly stuck. It was a breeze importing the data from Pro Football Reference and I had the tables added. I just needed to merge them all together in an full outer join to get the data in the same place so I can do the calculations. The issue is the merge button doesn't do full outer joins the way it needed to as far as I could tell. I tried different methods and none of them produced the result that I was looking for. I consulted the resource from the Power BI website and took the join query from there and adapted it to my needs. I also did the same for the creation of a new column for player names so everything was consistent. That was the simpliest and best way to do it in my mind. There may be an easier way to accomplish that. I don't think there's a way to join more than 2 tables, but that would've been helpful as I ended up needing to join a total of 4 tables to accomplish what I needed from the scoring formula I found. 

I ultimately decided because of time restraits that I would only be able to calculate the total fantasy scoring points for the whole season and display them in a stacked bar chart. It is dead simple to make a chart in Power BI. Something that I'm sure I would appreciate. Putting in the scoring formula was straight-forward. It's really nice how simple it is to make a new column and to input the formula or logic for it. 

![FF Stats](/FFStats.png)

This is what I ended up with. The points are a little off because I couldn't find an immediate way to get at fumbles recovered for a touchdown, but that shouldn't be a major factor in the scoring. I really wanted to do more, but I had time restraints with work and my personal life. I'm glad that I ended up with something that I could show off. 

I'm still determined and passionate. I actually enjoy dealing with statistics and algorithms so I think this could be a good fit for me. As for the projects I mentioned in my last post, I actually have a modded Minecraft server running in Docker and it's fully monitored by my Prometheus stack. I want to add some more game servers before I make a complete post on it, but that was really exciting to accomplish. 

