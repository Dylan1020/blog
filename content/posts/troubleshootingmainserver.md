---
title: "Troubleshooting my main hypervisor box"
date: 2019-12-30T21:50:37-06:00
draft: false
toc: false
images:
tags: 
---

I was planning on making a blog post after I got off work tonight. It just so happened that my main Proxmox box that I explain in my intro to my homelab post started making a really strange mechanical sound and then turned off. I decided to document my troubleshooting since that is most applicatable to the job I'm likely to get as my first step into my IT career. I'm not expecting to be able to manage servers right away, I will need to demostrate some form of help desk or technical support skills. I think this may be a blessing in disguise.

My first guess is that either my hard drive is failing or a cooling solution failed such as a fan. I haven't opened up the case yet. All that I know at this point is that it will POST and there's no burning smell or anything of that nature.

## Update one

It appears to be something with the power supply. It sounds like the fan is going out. I've tried to clean it out with canned air, but the same sound is persisting. The machine looks like it's working just fine other than that mechanical noise. I have ruled out the fan colliding with something. I'm going to start to really take it apart and I will be back with another update.

## Update two

After looking at it more, the issue is definitely the power supply. I'm not sure what the exact issue is, but it's not the fan. It probably is just on it's last legs. I don't want to push it too much since it's definitely the power supply. I'm not going to plug in back in. Going into this, I would have liked the troubleshooting to take longer and be an issue where I could potentially fix it. Now, I will just have to get a new power supply or just upgrade the machine like I've been wanting to. I'm sorry for such the short post, but it's getting late. I'm a little discouraged that this happened right when I was really starting to buckle down and work on projects. I'm am not deterred though. I'm committed and passionate in what I am doing.