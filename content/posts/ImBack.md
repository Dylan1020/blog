---
title: "I'm Back"
date: 2020-01-31T08:15:56-06:00
draft: false
toc: false
images:
tags: 
  - untagged
---

I got another hypervisor box! It's exactly the same as the other one, I just swapped the RAM and drives. It was nice to take a break even though I really want to work on stuff. I have some awesome stuff planned. I would like to setup a staging environment for this blog so I learn that part of GitlabCI and also Git branches, dockerized game servers so I get the experience of doing that, and another blog for Magic the Gathering. Magic the Gathering has been my game of choice for a very long time.

I didn't get the job I really wanted, but that has only increased my passion and dedication. This is what I want to do with my life. I say that repeatedly, because it's true. I want to make something out of my life and have a career I can be proud of. It's all about moving forward from disappointments. It's all we have.

This is going to be a short post, but I just wanted to get something out there. This is also my first post of the new year! 