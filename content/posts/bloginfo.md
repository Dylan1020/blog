---
title: "What is powering this blog?"
date: 2019-12-29T22:57:01-06:00
draft: false
toc: false
images:
tags: 
  - Hugo
  - AWS
  - s3
  - Gitlab
  - CI
  - CD
---

It would make sense for one of the first posts of the new blog is how the blog actually works and what I used to make it happen. I decided on Hugo as a blogging platform. It's fast, open-source, and it's built on Go which I am interested in as a language. Gitlab was the perfect fit as I intend to move everything over to Gitlab eventually and the built in Continous Integration and Delivery made it a no brainer. I originally was going to get a VPS to host the site using nginx, but I remembered the AWS free tier and that I wanted to tinker with AWS. AWS s3 amazed me at it's simplicity and I don't regret the decision one bit. It was very easy to deploy to through Gitlab CI/CD.

## Hugo

[Hugo](https://gohugo.io/) is by far my favorite blogging platform. There's a ton of themes, the theme I decided on is [hermit](https://themes.gohugo.io/hermit/). It was extremely easy to get started with and the live-preview is helpful. The extensive templating engine is a great feature as well. To create a new site, all that is required is a simple `hugo new site sitename` command and adding posts is a breeze too. `hugo new posts/postname.md`

`hugo server -D` will start the live server with drafts enabled so the edits can be viewed in real time even on drafts. To build the static site the command is `hugo -D` and it will create and/or populate the /public/ folder in your site. This made it simple to integrate with Gitlab CI/CD.

## Gitlab

[here](https://gitlab.com/Dylan1020/blog) is my git repository for the blog. You can view all the code and config files I have. I would like to go into detail of the `gitlab-ci.yml` file just to show how easy it is to get started with the CI/CD features of Gitlab. It's something that can be started at the beginning of every project and it will help the workflow immensly. Let's break it into parts

```YAML
stages:
    - build
    - deploy
```
This is how you define stages or different steps as I like to think of them. Obviously in this case, there will be a build step and a deploy step.

```YAML    
build:
    stage: build
    image: monachus/hugo
    only: 
    - master
    before_script:
    - git submodule sync --recursive
    - git submodule update --init --recursive
    script:
    - hugo version
    - hugo -d public_html
    artifacts:
        paths:
        - public_html
```
Here's the build step of the pipeline. 
* `image:` specifies what docker image to use. 
* `only:` is when the stage will run. In this case when a commit to master happens. This is how you can separate staging and production if need be. 
* `before_script:` commands to be ran before the build script happens. 
* `script:` the actual pipeline script that actually does the building.
* `artifacts:` whatever needs to be kept to send to the other stage. In this case the site folder that is build when the `hugo -d public_html` command is ran

```YAML    
deploy:
    stage: deploy
    image: xueshanf/awscli:latest
    only:
    - master
    script:
    - aws s3 sync public_html s3://your-bucket-name
```

This is how easy it is to deploy to AWS in the pipeline. On the AWS side, you do need to make a gitlabci user and give it permissions to modify the bucket. Also, it is neccessary to put the access key and secret key in the repository variables.

As an aside, one of the things that I wish I learned in my education was git. It would've made my life so much easier in my programming courses, especially when it came to group projects.

## AWS

The site lives on a simple s3 bucket on the AWS free tier. It was fairly easy to set up even for someone who never used AWS before. The documentation is intuitive and educational as well. I think I will experiment with AWS for other things. I am particarly interested in AWS Lambda, but I'm not sure what my use case would be for that at the current moment. I recently read `The Pheonix Project` by Gene Kim and in the book the characters discover that the data processing that they need to be able to compete with their competitor takes much longer than realized. They decide to just spin up more virtual servers that are completely automated. AWS Lambda would be a perfect use case I would imagine instead of trying to spin up tens of virtual servers.

## Conclusion

All in all, I had a ton of fun setting this up and I learned quite a bit especially with regards to AWS which I never used before. It took me a few hours to set up from scratch and have it be completely automated. I did it over my weekend that I only worked half days. I strived to get it done in time for my job interview, just in case it comes up. Hugo and Gitlab allowed me to achieve that faster than I imagined. It will be dead simple to add more posts as I continue my journey of education and learning. 