---
title: "Long Time No See"
date: 2020-08-02T09:49:22-02:00
draft: false
toc: false
---

It really has been a long time for me to not make a post on here. I've since achieved my goal of getting an IT job, COVID has happened, and I've just been super busy that I haven't had a chance to update this much. I'm not sure where to take it from here honestly, but I would like to still make posts here and there. I've made some cool stuff with Powershell at my job, but I'm unfortunately not able to open source it or anything like that as far as I'm aware, but I might make a post on what I've created and maybe some screenshots. Nonetheless, I hope everybody is doing well and I might see you again. So long!