---
title: "Introduction to my homelab"
date: 2019-12-29T09:49:03-06:00
draft: false
toc: false
images:
tags: 
  - proxmox
  - prometheus
  - grafana
  - docker
---

In this post, I will document what I have in my home lab, what I currently have running, and what I hope to achieve with it.

# Hardware

## Main Proxmox Box

My main Proxmox box is a HP Compaq Elite 8300 SFF that I got for fairly cheap on Amazon. I upgraded the ram and storage space a bit. I haven't pushed it too much yet, but it runs my prometheus stack on Docker just fine. I eventually will migrate my Minecraft server over. I intend for this to be my main hypervisor and docker box.

* i5-3570 quad core
* 24GB DDR3 RAM
* 500GB SSD 
* 500GB HDD

## Secondary Proxmox Box

This was originally my gaming laptop a few years ago. It's an Alienware 13 that I repurposed to serve as my first hypervisor two years ago. Right now it's where my Minecraft server lives. It also contains my dev environment that I hook into through VS Code. I work on my blog through VS Code connecting to my dev environment and that's also where my Python projects live. My main one is to export custom Prometheus metrics from my Minecraft world.

* i5-421OU quad core
* 8GB RAM
* 1TB HDD

## Pihole Raspberry Pi

I have a Raspberry Pi 3 that I am running PiHole on. It serves as my DNS and DHCP for my network. I have always been a fan of Raspberry Pis.

# What I'm running and using currently

### Prometheus Stack

   [This](https://github.com/vegasbrianc/prometheus) is what I'm using currently for my Prometheus Stack. I originally had it installed in LXC containers on my main Proxmox Box, but I also wanted to learn Docker and this seemed like a perfect fit. 
   I have added a endpoint to scrape that is my minecraft server and metrics inside of Minecraft.

### Gitlab

   I have ran this locally in the past, but now I just use the web version. It supports everything I could want to have. I also prefer it over Github because of the built-in Continous Integration and Deployment features and the fact that it is open-source. Github Actions are looking pretty interesting though.

### AWS

   I use AWS s3 to host this blog and it is automatically updated when I push a commit to the repository on Gitlab

   I would like to continue to experiment with AWS. I was pleasantly surprised with how easy s3 was to use.

### Digital Ocean Droplet

   I am the administrator for a game server that is hosted on a Digital Ocean Droplet. I created a systemd unit for it so it integrates better and it will automatically restart if it needs to.

# Future Goals

My goals for the future are a number of things with the ultimate end goal of achieving a career in IT

### Learn a logging stack.

   I am very interested in [Loki](https://grafana.com/oss/loki/) since that will integrate perfectly with my existing Prometheus/Grafana stack. 

   I have also looked into ELK and Graylog.

### Learn a Configuration Management Tool

   I really like how Ansible looks and have done some reading and listened to talks on it. I have messed with it briefly, but nothing to show off currently.

   I have also looked at SaltStack and Terraform.

